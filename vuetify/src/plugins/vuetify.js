import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import VueI18n from "../i18n";

Vue.use(Vuetify);

import en from 'vuetify/es5/locale/en'
import sv from 'vuetify/es5/locale/sv'

export default new Vuetify({
    lang: {
        locales: {en, sv},
        current: VueI18n.locale
    },
    theme: {
        themes: {
            light: {
                primary: '#41B883'
            },
            dark: {
                primary: '#34495E',
            }
        }
    }
});
