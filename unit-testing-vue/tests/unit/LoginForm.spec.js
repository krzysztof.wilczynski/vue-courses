import LoginForm from "../../src/components/LoginForm";
import {mount} from "@vue/test-utils";
import {
    toBeVisible
} from '@testing-library/jest-dom/matchers'

expect.extend({toBeVisible})

describe('LoginForm', () => {
    it('Emits an event with a user data payload', () => {
        const wrapper = mount(LoginForm)
        const input = wrapper.find('input[data-testid="name-input"]')

        input.setValue('Marzenna Kępka')
        wrapper.trigger('submit')

        const formSubmittedCalls = wrapper.emitted('formSubmitted')
        expect(formSubmittedCalls).toHaveLength(1)

        const expectedPayload = {name: 'Marzenna Kępka'}
        expect(wrapper.emitted('formSubmitted')[0][0]).toMatchObject(expectedPayload)


    })
})