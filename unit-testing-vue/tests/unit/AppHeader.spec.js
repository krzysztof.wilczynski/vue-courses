import AppHeader from "../../src/components/AppHeader";
import {mount} from "@vue/test-utils";
import {
    toBeVisible
} from '@testing-library/jest-dom/matchers';

expect.extend({toBeVisible})

describe('AppHeader', () => {
    test('If user is not logged in, do not show logout button', async () => {
        const wrapper = mount(AppHeader)
        expect(wrapper.find('button').element).not.toBeVisible()
    })

    test('If user is logged in, show logout button', async () => {
        const wrapper = mount(AppHeader)
        wrapper.setData({loggedIn: true})
        await wrapper.vm.$nextTick()
        expect(wrapper.find('button').element).toBeVisible()
    })
})

/*
1. Create a test suite (a block of tests) -> describe(...)
2. Set up your tests -> test(...)
3. Mount the component with vue-test-utils -> mount(...)
4. Set data, if necessary -> SetData(...)
5. Assert what the result should be -> expect(...)
 */